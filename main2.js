$("#validar").click(function(){
    const user1 = $("#1 option:selected").val()
    const user2 = $("#2 option:selected").val()
    //console.log(user1)

    //front
    $("#iteracion").empty()


    //pesos
    var pesos = [0,0]

    //funcion activación
    activacion(user1, user2, pesos)
    
    
  });


//activación
  function activacion(item1,item2, pesos){

    if($("#iteracion").text() == ""){
        $("#iteracion").text(1)
    }else{
        let iteracion = $("#iteracion").text()
        let ite2 =  parseInt(iteracion) + 1;
        $("#iteracion").text(ite2)
    }

    let tasaaprendizaje = 1;
         
  

    var suma = (item1 * pesos[0]) + (item2 * pesos[1])
    


    //console.log(suma)


    let salidareal = 0;
    let salidaesperada = 0;
    let aprendizaje = 0;
    


    if(suma > 0){
        salidareal = 1; //y(0)
    }else if(suma <= 0){
        salidareal = -1 //y(0)
    }

    if(item1 == 1 && item2 == 1){
        salidaesperada = 1;
    }else{
        salidaesperada = -1;
    }
   
//console.log(salidareal)
//console.log(salidaesperada)

    if(salidareal == salidaesperada){
        if(salidaesperada >= 1){
           $("#rta").text("Conectados")
       }else{
        $("#rta").text("Desconectados")
       }
    }else{
        hebb(item1, item2, pesos, salidareal, tasaaprendizaje)
    }


 
  }


  //funcion para recalcular los pesos
  function hebb(item1,item2, pesos, salidareal, tasa){

    let variacion = 0;
    let conexiones = [item1,item2]

    let pesosnew = new Array()

    for(let p = 0; p < conexiones.length; p++){
        variacion = tasa * salidareal * conexiones[p]
        //console.log(variacion)
        pesosnew[p] = pesos[p] + variacion;
        //console.log(pesosnew)
    }
    
    console.log(pesosnew)
    activacion(item1,item2, pesosnew)
  }
