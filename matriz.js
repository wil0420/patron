$("#validar").click(function(){
    const señal = $("#1 option:selected").val()
    

    if(señal == 1){
        crearMatrizStop()
    }else if(señal == 2){
        crearMatrizgo()
    }
    
   
    
  });


  //matriz Stop
  function crearMatrizStop(){

    let fila = 15;
    let columna = 5;

    //
    var stop = {
        0:  [1,1,1,0,1,1,1,0,1,1,1,0,1,1,1], 
        1 : [1,0,0,0,0,1,0,0,1,0,1,0,1,0,1],
        2 : [1,1,1,0,0,1,0,0,1,0,1,0,1,1,1],
        3 : [0,0,1,0,0,1,0,0,1,0,1,0,1,0,0],
        4 : [1,1,1,0,0,1,0,0,1,1,1,0,1,0,0]
        }


    const salidasstop = {
        0:  [1,1,1,0,1,1,1,0,1,1,1,0,1,1,1], 
        1 : [1,0,0,0,0,1,0,0,1,0,1,0,1,0,1],
        2 : [1,1,1,0,0,1,0,0,1,0,1,0,1,1,1],
        3 : [0,0,1,0,0,1,0,0,1,0,1,0,1,0,0],
        4 : [1,1,1,0,0,1,0,0,1,1,1,0,1,0,0]
        }


    var pesos = [-1000,-1000,-1000,-1000,-1000]

    for(let i = 0; i < columna; i++){
        for(let p = 0; p < fila; p++){
             //mostrar figura vista
            if(p<1){
            var node = document.createElement("li"); 
            }
            var textnode = document.createElement("span"); 
            if(stop[i][p] == 1){
                textnode.classList.add("rojo");
            }
            textnode.append(stop[i][p])    
            node.appendChild(textnode);                              
            document.getElementById("lista").appendChild(node);
        }
    }
    vectorsalidas = salidasvectores(stop,salidasstop,fila)
    console.log(vectorsalidas)

    activacion(vectorsalidas,pesos,fila)
  }


  //matriz GO
  function crearMatrizgo(){

    let fila = 7;
    let columna = 5;

    var go = {
        0:  [1,1,1,0,1,1,1], 
        1 : [1,0,0,0,1,0,1],
        2 : [1,1,1,0,1,0,1],
        3 : [1,0,1,0,1,0,1],
        4 : [1,1,1,0,1,1,1]
        } 

        const salidasgo = {
            0:  [1,1,1,0,1,1,1], 
            1 : [1,0,0,0,1,0,1],
            2 : [1,1,1,0,1,0,1],
            3 : [1,0,1,0,1,0,1],
            4 : [1,1,1,0,1,1,1]
            }     

        var pesos = [-1000,-1000,-1000,-1000,-1000]


    for(let i = 0; i < columna; i++){
        for(let p = 0; p < fila; p++){
               //mostrar figura vista
            if(p<1){
            var node = document.createElement("li"); 
            }
            var textnode = document.createElement("span"); 
            if(go[i][p] == 1){
                textnode.classList.add("rojo");
            }
            textnode.append(go[i][p])    
            node.appendChild(textnode);                              
            document.getElementById("lista").appendChild(node);
        }
    }

    vectorsalidas = salidasvectores(go,salidasgo,fila)
    console.log(vectorsalidas)

    activacion(vectorsalidas,pesos,fila)

  }



  function activacion(vector,pesos,long){
    //console.log(matriz)
   console.log(pesos)

    if($("#iteracion").text() == ""){
        $("#iteracion").text(1)
    }else{
        let iteracion = $("#iteracion").text()
        let ite2 =  parseInt(iteracion) + 1;
        $("#iteracion").text(ite2)
    }


    let tasaaprendizaje = 1;

    var textosalida ="";
    var suma = 0;

    if(long > 7){
        textosalida = "Es un Stop"
        suma = operar(vector,pesos)
        console.log(suma)
    }else{
        textosalida = "Es un Go"
        suma = operar(vector,pesos)
    }   


    //v(O)


    let salidareal = 0;
    let salidaesperada = 0;
    let aprendizaje = 0;

    //salidas real
    if(suma < 0){
        salidareal = -1;
    }else{
        salidareal = 1;
    }

    
    //salida deseada
    let verificar = 0;
    for(let v=0; v<5; v++){
        verificar += vector[v]
        
    }
    console.log(verificar)
    if(verificar > 1){
        salidaesperada = 1;
    }if(verificar <= 1){
        salidaesperada = -1;
    }
    
        //error
        aprendizaje = parseInt(salidaesperada) - parseInt(salidareal);

    if(aprendizaje == 0){
        if(salidaesperada >= 0){
            $("#rta").text(textosalida)
        }else{
            $("#rta").text("No es")
        }
           
    }else{
        recalcular(vector, pesos, aprendizaje, tasaaprendizaje,long)
    }

  }



  //funcion para recalcular los pesos
  function recalcular(vector, pesos, aprendizaje, tasa,longitud){
    let variacion = 0;
    let pesosnew = new Array()
    for(let a = 0; a < vector.length; a++){
        variacion = tasa * aprendizaje * vector[a]
        pesosnew[a] = pesos[a] + variacion;
        console.log(pesosnew)
    }

    
   // console.log(pesos)
    activacion(vector, pesosnew,longitud)
  }



  //operar salida deseada
  function salidasvectores(matriz, matrizenviada, longitud){
      var limite = 0;
    if(longitud == 15){
        limite = 12;
    }else{
        limite = 5;
    }
    let totalsalida = 0;
    let salidas = new Array()
    for(let o=0; o < 5; o++ ){
        totalsalida = 0;
        for(let f=0; f<longitud; f++){
            if(matriz[o][f] == matrizenviada[o][f]){
                totalsalida += 1;
            }
            if(f>=4){
                if(totalsalida >= limite){
                    salidas[o] = 1;
                }else{
                    salidas[o] = -1;
                }
            }

            
        }
        
    }

//console.log(salidas)//40
return salidas;
}



  //operar matrices
  function operar(matriz,pesos){
    let total = 0;
    let sumaope = 0;

    for(let i = 0; i<5; i++){
           total = matriz[i] * pesos[i] 
           sumaope = sumaope + total;
    }
    return sumaope;
    

  }